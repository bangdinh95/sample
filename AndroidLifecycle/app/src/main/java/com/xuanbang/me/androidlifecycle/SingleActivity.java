package com.xuanbang.me.androidlifecycle;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


/**
 * ========== SINGLE ACTIVITY ==========0364 653 273
 * <p>
 * =>>> Trường hợp 1: là bấm nút back (pressback mặc định) thì vòng đời Android
 * onCreate(null) -> onStart() -> onResume() -> [..Press Back..or call method Activity.finish()] -> onPause() -> onStop() -> onDestroy().
 * {..App restart...} onCreate(null) -> onStart() -> onResume().
 * <p>
 * =>>> Trường hợp 2: bấm vào phím Home
 * onCreate(null) -> onStart() -> onResume() -> [..Button Home or minimize app] -> onPause() -> onStop() -> onSaveInstanceState().
 * {..App restart..} onRestart() -> onStart() -> onResume().
 * <p>
 * =>>> Trường hợp 3: Xoay màn hình
 * onCreate() -> onStart() -> onResume() -> [..Xoay..] -> onPause() -> onStop() -> onSaveInstanceState() -> onDestroy() -> onCreate(Bundule) -> onStart() -> onRestoreInstance() -> onResume().
 * <p>
 * =>>> Trường hợp 4: App is pause (multi window started)
 * onCreate() -> onStart() -> onResume() -> [...multi window started...] -> onPause() -> {...focus comes back to app...} -> onResume().
 */
public class SingleActivity extends AppCompatActivity {

    private final static String TAG = SingleActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        Log.e(TAG, "onCreate()");

        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.frame_contain);
        if (fragment == null) {
            Log.e(TAG, "onCreate() fragment == null");
            getSupportFragmentManager().beginTransaction().add(R.id.frame_contain, new SingleFragment()).commit();
        }else {
            Log.e(TAG, "onCreate() fragment != null");
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart()");
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        Log.e(TAG, "onRestoreInstanceState() - PersistableBundle");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "onRestoreInstanceState() " + savedInstanceState.getString("KEY"));

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        Log.e(TAG, "onSaveInstanceState() - PersistableBundle");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState()");
        outState.putString("KEY", "KEY Single");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy()");
    }
}
