package com.bang.me.androidosample

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bang.me.androidosample.databinding.ActivityMainBinding
import com.bang.me.androidosample.intent.IntentActivity
import com.bang.me.androidosample.notification.NotificationActivity
import android.app.SearchManager
import android.net.Uri


class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null
    private val TAG = MainActivity::class.qualifiedName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.button_notification -> {
                startActivity(Intent(this@MainActivity, NotificationActivity::class.java))
            }
            R.id.button_intent -> {
                startActivity(Intent(this@MainActivity, IntentActivity::class.java))
            }
        }
    }
}
