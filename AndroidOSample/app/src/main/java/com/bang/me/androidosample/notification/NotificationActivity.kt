package com.bang.me.androidosample.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.databinding.DataBindingUtil
import com.bang.me.androidosample.R
import com.bang.me.androidosample.databinding.ActivityNotificationBinding
import com.google.android.material.snackbar.Snackbar

// Used for Notification Style array and switch statement for Spinner selection.
private const val BIG_TEXT_STYLE = "BIG_TEXT_STYLE"
private const val BIG_PICTURE_STYLE = "BIG_PICTURE_STYLE"
private const val INBOX_STYLE = "INBOX_STYLE"
private const val MESSAGING_STYLE = "MESSAGING_STYLE"

private val NOTIFICATION_STYLES_DESCRIPTION = arrayOf(
    "Demos reminder type app using BIG_TEXT_STYLE",
    "Demos social type app using BIG_PICTURE_STYLE + inline notification response",
    "Demos email type app using INBOX_STYLE",
    "Demos messaging app using MESSAGING_STYLE + inline notification responses"
)

class NotificationActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var binding: ActivityNotificationBinding? = null
    private var notificationId: Int = 0
    private val TAG = NotificationActivity::class.qualifiedName
    private val groupIdNotify = "idNotify"
    private val groupIdIndicator = "idIndicator"

    private lateinit var groupNameNotify: String
    private lateinit var groupNameIndicator: String
    private var notifiManager: NotificationManager? = null


    private val NOTIFICATION_STYLES = arrayOf(
        BIG_TEXT_STYLE,
        BIG_PICTURE_STYLE,
        INBOX_STYLE,
        MESSAGING_STYLE
    )
    private var positionSelected = 0
    private lateinit var notificationManagerCompat: NotificationManagerCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification)
        setupToolbar()
        setupSpinner()
        notificationManagerCompat = NotificationManagerCompat.from(application)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            groupNameNotify = getString(R.string.group_name_notify)
            groupNameIndicator = getString(R.string.group_name_indicator)

        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding?.toolbar)
        supportActionBar?.title = getString(R.string.app_name)
    }

    private fun setupSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout.
        val adapter = ArrayAdapter(
            this@NotificationActivity,
            android.R.layout.simple_spinner_item,
            NOTIFICATION_STYLES
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerStyle.adapter = adapter
        binding!!.spinnerStyle.onItemSelectedListener = this@NotificationActivity
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        positionSelected = position
        binding!!.tvShowStyle.text = NOTIFICATION_STYLES_DESCRIPTION[position]
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btn_create_notification -> {
                var areNotificationsEnable = notificationManagerCompat.areNotificationsEnabled()

                if (!areNotificationsEnable) {
                    val snackbar = Snackbar.make(
                        binding!!.container,
                        "You need to enable notifications for app",
                        Snackbar.LENGTH_LONG
                    ).setAction("ENABLE") {
                        openNotificationSettingForApp()
                    }
                    snackbar.show()
                    return
                }

                var notifyStyle = NOTIFICATION_STYLES[positionSelected]
                with(notifyStyle) {
                    when {
                        contains(BIG_TEXT_STYLE) -> {
                            Log.e(TAG, "BIG_TEXT_STYLE")
//                            generateBigTextStyleNotification()
                            createNotificationChannel("channel_1")
                        }
                        contains(BIG_PICTURE_STYLE) -> {
                            Log.e(TAG, "BIG_PICTURE_STYLE")
                            generateBigPictureStyleNotification()
                        }
                        contains(INBOX_STYLE) -> {
                            Log.e(TAG, "INBOX_STYLE")
                            generateInboxStyleNotification()
                        }
                        else -> {
                            Log.e(TAG, "MESSAGING_STYLE")
                            generateMessagingStyleNotification()
                        }
                    }
                }

//                createNewNotification("chat_call", groupIdNotify)

            }
        }
    }

    private fun generateBigTextStyleNotification() {
//        var bigTextStyleReminderAppData: MockDataBase.BigTextStyleReminderAppData
    }

    private fun generateBigPictureStyleNotification() {

    }

    private fun generateInboxStyleNotification() {}
    private fun generateMessagingStyleNotification() {}

    private fun openNotificationSettingForApp() {

        var intent = Intent("android.settings.APP_NOTIFICATION_SETTINGS")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // for Android 8 and above
            intent.putExtra("android.provider.extra.APP_PACKAGE", packageName)
        } else {
            //for Android 5-7
            intent.putExtra("app_package", packageName)
            intent.putExtra("app_uid", applicationInfo.uid)
        }
        startActivity(intent)
    }

    private fun createNotificationChannel(CHANNEL_ID: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.group_name_notify)
            val descriptionText = getString(R.string.demo_notification)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_message_black_24dp)
            .setContentTitle("My notification")
            .setContentText("Hello World!")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            // Set the intent that will fire when the user taps the notification
//            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        notificationId++
        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId, builder.build())
        }

    }

}

