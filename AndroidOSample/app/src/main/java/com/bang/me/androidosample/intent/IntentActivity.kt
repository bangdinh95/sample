package com.bang.me.androidosample.intent

import android.app.SearchManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bang.me.androidosample.R
import com.bang.me.androidosample.databinding.ActivityIntentBinding

class IntentActivity : AppCompatActivity() {
    private lateinit var bindingView: ActivityIntentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this@IntentActivity, R.layout.activity_intent)
        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(bindingView.toolbar)
        supportActionBar!!.title = getString(R.string.name_activities_intent)
    }

    private fun sentAllAppOther() {
        var intent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "*/*"
        }
        startActivity(intent)
    }

    private fun sentMailTo(emailFrom: String) {
        var intent = Intent().apply {
            action = Intent.ACTION_SENDTO
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, emailFrom)

            putExtra(Intent.EXTRA_SUBJECT, "Thư mời tham gia tri ân khách hàng")
            putExtra(Intent.EXTRA_TEXT, "Tôi muốn thông báo với bạn rằng")
            putExtra(Intent.EXTRA_CC, "bangdx@gmail.com")
            putExtra(Intent.EXTRA_BCC, "bangdx95@gmail.com")
            putExtra(Intent.EXTRA_BCC, "bangdxit@gmail.com")

        }
        startActivity(Intent.createChooser(intent, "Choose an email client from..."))
    }

    private fun sentQueryWebSearch() {
        val q = "L.Messi"
        val intent = Intent(Intent.ACTION_WEB_SEARCH).apply {
            putExtra(SearchManager.QUERY, q)
        }
        startActivity(intent)
    }

    private fun phoneCall() {
        val intent = Intent().apply {
            action = Intent.ACTION_DIAL//Mở ứng dụng gọi điện
            //Intent.ACTION_CALL gọi trực tiếp đến số điện thoại yêu cầu
        }
        startActivity(intent)
    }


    private fun phoneCallNumber(number: String) {
        /**
         * Check permission call phone first call
         * Số điện thoại hợp lệ được định nghĩa trong @link IETF RFC 3966. Các ví dụ sau là hợp lệ
        tel:2125551212
        tel:(212) 555 1212
         */
        val intent = Intent().apply {
            action = Intent.ACTION_CALL
            data = Uri.parse("tel:$number")
        }
        if (intent.resolveActivity(packageManager) != null)
            startActivity(intent)
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.button_sent -> {
                sentAllAppOther()
            }
            R.id.button_sent_mail_to -> {
                var emailFrom = "bangdinh95@gmail.com"
                sentMailTo(emailFrom)
            }
            R.id.button_call -> {
                phoneCall()
            }
            R.id.button_call_number -> {
                phoneCallNumber("0966362381")
            }
            R.id.button_web_search -> {
                sentQueryWebSearch()
            }

        }
    }
}