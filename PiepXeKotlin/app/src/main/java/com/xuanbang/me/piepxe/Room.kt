package com.xuanbang.me.piepxe

interface Room {
    fun bar() = println("Room - bar")

    fun foo() = println("Room - foo")
}