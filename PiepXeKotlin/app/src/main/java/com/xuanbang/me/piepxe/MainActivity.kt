package com.xuanbang.me.piepxe

import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Double.sum


class MainActivity : AppCompatActivity() {

    var TAG: String = MainActivity::class.simpleName.toString()

    var listUser: IntArray = intArrayOf(1, 2, 3, 4)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv.paintFlags = tv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

    }


    class Person(var name: String? = null, var address: Address? = null) {
        override fun toString(): String {
            return GsonBuilder().create().toJson(this)
        }
    }

    class Address(var address: String? = null) {
        override fun toString(): String {
            return GsonBuilder().create().toJson(this)
        }
    }

    fun person(block: Person.() -> Unit): Person = Person().apply(block)
    fun Person.address(block: Address.() -> Unit) {
        address = Address().apply(block)
    }


    val person = person {
        name = "John"
        address {
            address = "Hoài Ân - Bình Định"
        }

        val vec = Vec(2f, 3f) + Vec(4f, 5f)

        println("Vector plus $vec")

        var ship: String? = null
        var captain = ""
        var name = ""
        val nameDe = ship ?: "unknown"

        var res = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sum(2.0, 4.0)
        } else {
            TODO("VERSION.SDK_INT < N")
        }
        println("Res $res")
    }

    data class Vec(var a: Float, var b: Float) {
        operator fun plus(v: Vec) = Vec(a + v.a, b + v.b)
        override fun toString(): String {
            return GsonBuilder().create().toJson(this)
        }
    }

}
