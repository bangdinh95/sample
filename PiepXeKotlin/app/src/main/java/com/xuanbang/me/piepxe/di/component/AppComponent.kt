package com.xuanbang.me.piepxe.di.component

import android.app.Application
import com.xuanbang.me.piepxe.PiepXeApp
import dagger.BindsInstance
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@dagger.Component(
    modules = [
        (AndroidSupportInjectionModule::class)
    ]
)
interface AppComponent : AndroidInjector<PiepXeApp> {
    @dagger.Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}