package com.xuanbang.me.piepxe

import java.util.*

open abstract class SV {
    private var id: Int = 0

    private var name: String? = null

    private var birthday: Date? = null

    var Id: Int
        get() {
            return id
        }
        set(value) {
            id = value
        }

    var Name: String?
        get() {
            return this.name
        }
        set(value) {
            name = value
        }

    var birthDay: Date?
        get() {
            return birthDay
        }
        set(value) {
            birthday = value
        }
}