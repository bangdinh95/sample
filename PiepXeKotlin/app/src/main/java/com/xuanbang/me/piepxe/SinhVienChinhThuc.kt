package com.xuanbang.me.piepxe

import com.google.gson.GsonBuilder
import java.util.*

class SinhVienChinhThuc() : SV(), Room, Room2, Cloneable {
    
    constructor(id: Int, name: String, birthday: Date) : this() {
        this.Id = id
        this.Name = name
        this.birthDay = birthday
    }


    override fun bar() {
        println("Room and Room2 -> bar")
    }

    override fun foo() {
        super<Room>.foo()
        super<Room2>.foo()

    }

    override fun toString(): String {
        return GsonBuilder().create().toJson(this)
    }

    fun copy(): SV {
        var sv: SinhVienChinhThuc = clone() as SinhVienChinhThuc
        return sv
    }

}

interface B {
    fun foo()
}