package com.xuanbang.me.piepxe.di.component

import com.xuanbang.me.piepxe.MainActivity

@dagger.Component
interface MagicBox {
    fun poke(activity: MainActivity)
}
