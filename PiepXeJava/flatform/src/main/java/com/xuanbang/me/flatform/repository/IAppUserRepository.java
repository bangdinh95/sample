package com.xuanbang.me.flatform.repository;


import com.xuanbang.me.flatform.entities.user.User;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface IAppUserRepository {
    Flowable<User> getUser(String usernId);

    Flowable<User> getUser();

    Single<String> getIdUserSharedPrefs();

}
