package com.xuanbang.me.piepxe.utils;

import org.jetbrains.annotations.NotNull;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;

public final class RxBus {

    private final static BehaviorSubject<Object> behaviorSubject = BehaviorSubject.create();

    public static Disposable subscribe(@NotNull Consumer<Object> action) {
        return behaviorSubject.subscribe(action);
    }

    public static void publish(@NotNull Object message) {
        behaviorSubject.onNext(message);
    }
}
