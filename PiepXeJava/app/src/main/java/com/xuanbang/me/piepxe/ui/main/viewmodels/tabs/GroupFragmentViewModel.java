package com.xuanbang.me.piepxe.ui.main.viewmodels.tabs;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.xuanbang.me.piepxe.Resource;
import com.xuanbang.me.piepxe.common.base.views.BaseViewModel;

import javax.inject.Inject;

public class GroupFragmentViewModel extends BaseViewModel {
    private MutableLiveData<Resource<String>> mLiveData;

    @Inject
    public GroupFragmentViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Resource<String>> getLiveData() {
        if (mLiveData != null)
            return mLiveData;
        mLiveData = new MutableLiveData<>();
        mLiveData.setValue(Resource.loading(null));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLiveData.setValue(Resource.success(null));
            }
        }, 2000);
        return mLiveData;
    }
}
