package com.xuanbang.me.piepxe.common.base.inf;

public interface OnBackPressHandel {
    boolean onBackPressed();
}
