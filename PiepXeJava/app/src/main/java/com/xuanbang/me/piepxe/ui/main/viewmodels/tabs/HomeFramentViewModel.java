package com.xuanbang.me.piepxe.ui.main.viewmodels.tabs;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.xuanbang.me.piepxe.Resource;
import com.xuanbang.me.piepxe.common.base.views.BaseViewModel;

import javax.inject.Inject;

public class HomeFramentViewModel extends BaseViewModel {

    private MutableLiveData<Resource<String>> mResourceLiveData;
    private Application mApplication;

    @Inject
    public HomeFramentViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
    }


    public LiveData<Resource<String>> getLiveDataHome() {
        if (mResourceLiveData != null) {
            return mResourceLiveData;
        }
        mResourceLiveData = new MutableLiveData<>();
        mResourceLiveData.setValue(Resource.loading(null));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mResourceLiveData.setValue(Resource.success(null));
            }
        }, 2000);
        return mResourceLiveData;
    }
}
