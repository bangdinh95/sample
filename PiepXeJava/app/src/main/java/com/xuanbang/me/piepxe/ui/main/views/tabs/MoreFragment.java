package com.xuanbang.me.piepxe.ui.main.views.tabs;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.xuanbang.me.piepxe.R;
import com.xuanbang.me.piepxe.Resource;
import com.xuanbang.me.piepxe.common.base.views.BaseFragmentMVVM;
import com.xuanbang.me.piepxe.common.base.views.modules.BaseFragmentModule;
import com.xuanbang.me.piepxe.databinding.FragmentMoreBinding;
import com.xuanbang.me.piepxe.di.scopes.PerFragmentScoped;
import com.xuanbang.me.piepxe.ui.home.views.HomeFragment;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.MoreFragmentViewModel;
import com.xuanbang.me.piepxe.ui.navigators.Navigation;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Binds;

public class MoreFragment extends BaseFragmentMVVM<FragmentMoreBinding, MoreFragmentViewModel> {

    public final static String TAG = MoreFragment.class.getSimpleName();

    @Inject
    Navigation mNavigation;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_more;
    }

    @Override
    public void initViewModel(ViewModelProvider.Factory factory) {

        mViewModel = ViewModelProviders.of(this, factory).get(MoreFragmentViewModel.class);
        // mNavigation.startActivityHome(getActivity(), new HomeActivity());
    }


    @Override
    public void initData(Bundle saveInstanceState) {
        addChildFragment(R.id.frame_contain_child, new HomeFragment());
        mViewModel.getLiveData().observe(this, new Observer<Resource<String>>() {
            @Override
            public void onChanged(Resource<String> stringResource) {
                mViewDataBinding.setResource(stringResource);
            }
        });
    }


    /**
     * Provides main fragment dependencies.
     */
    @dagger.Module(includes = {
            BaseFragmentModule.class
    })
    public abstract static class Module {

        /**
         * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
         * fragment modules, which must provide a concrete implementation of {@link Fragment}
         * and named {@link BaseFragmentModule#FRAGMENT}.
         *
         * @param fragment the main fragment
         * @return the fragment
         */
        @Named(BaseFragmentModule.FRAGMENT)
        @PerFragmentScoped
        @Binds
        abstract Fragment baseFragment(MoreFragment fragment);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated");
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e(TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

}
