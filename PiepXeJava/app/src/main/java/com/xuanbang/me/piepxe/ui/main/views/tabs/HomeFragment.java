package com.xuanbang.me.piepxe.ui.main.views.tabs;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.xuanbang.me.piepxe.R;
import com.xuanbang.me.piepxe.Status;
import com.xuanbang.me.piepxe.common.base.views.BaseFragmentMVVM;
import com.xuanbang.me.piepxe.common.base.views.modules.BaseFragmentModule;
import com.xuanbang.me.piepxe.databinding.FragmentHomeBinding;
import com.xuanbang.me.piepxe.di.scopes.PerFragmentScoped;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.HomeFramentViewModel;
import com.xuanbang.me.piepxe.ui.main.views.FragmentDependencyHome;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Binds;

/**
 * A simple {@link Fragment} subclass.
 */

public class HomeFragment extends BaseFragmentMVVM<FragmentHomeBinding, HomeFramentViewModel> {

    public final static String TAG = HomeFragment.class.getSimpleName();
    @Inject
    FragmentDependencyHome mFragmentDependencyHome;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initData(Bundle saveInstanceState) {
        mViewModel.getLiveDataHome().observe(this, stringResource -> {
            Log.e(TAG, "onChanged");
            mViewDataBinding.setResource(stringResource);
            mViewDataBinding.executePendingBindings();
            if (stringResource.status.equals(Status.SUCCESS)) {
                mViewDataBinding.tvHome.setText(getString(R.string.title_home));
            }
        });
    }

    @Override
    public void initViewModel(ViewModelProvider.Factory factory) {
        mViewModel = ViewModelProviders.of(this, factory).get(HomeFramentViewModel.class);
    }

    @dagger.Module(includes = {BaseFragmentModule.class})
    public abstract static class Module {

        @Named(BaseFragmentModule.FRAGMENT)
        @PerFragmentScoped
        @Binds
        abstract androidx.fragment.app.Fragment bindFragment(HomeFragment fragment);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated");
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e(TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

}
