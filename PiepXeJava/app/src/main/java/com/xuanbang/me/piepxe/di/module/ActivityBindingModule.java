package com.xuanbang.me.piepxe.di.module;

import com.xuanbang.me.piepxe.di.scopes.PerActivityScoped;
import com.xuanbang.me.piepxe.ui.login.views.LoginAccountActivity;
import com.xuanbang.me.piepxe.ui.main.views.MainActivity;
import com.xuanbang.me.piepxe.ui.started.views.StartedActivity;

import dagger.android.ContributesAndroidInjector;

@dagger.Module
public abstract class ActivityBindingModule {

    @PerActivityScoped
    @ContributesAndroidInjector(modules = {StartedActivity.Module.class})
    abstract StartedActivity bindNullMainActivity();

    @PerActivityScoped
    @ContributesAndroidInjector(modules = {
            MainActivity.Module.class
    })
    abstract MainActivity bindMainActivity();

    @PerActivityScoped
    @ContributesAndroidInjector(modules = {LoginAccountActivity.Module.class})
    abstract LoginAccountActivity bindLoginAccountActivity();

}
