package com.xuanbang.me.piepxe.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.xuanbang.me.piepxe.di.BaseModelFactory;
import com.xuanbang.me.piepxe.di.key.ViewModelKey;
import com.xuanbang.me.piepxe.ui.login.viewmodels.LoginAccountActivityViewModel;
import com.xuanbang.me.piepxe.ui.login.viewmodels.LoginAccountFragmentViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.MainActivityViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.GroupFragmentViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.HomeFramentViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.MessageFragmentViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.MoreFragmentViewModel;
import com.xuanbang.me.piepxe.ui.main.viewmodels.tabs.ProfileFragmentViewModel;
import com.xuanbang.me.piepxe.ui.started.viewmodels.StartedActivityViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(BaseModelFactory factory);

    /* ---- Started --- */
    @Binds
    @IntoMap
    @ViewModelKey(StartedActivityViewModel.class)
    abstract ViewModel bindNullMainActivityViewModel(StartedActivityViewModel repoViewModel);

    /*---- Login - Register ---*/
    @Binds
    @IntoMap
    @ViewModelKey(LoginAccountActivityViewModel.class)
    abstract ViewModel bindLoginAccountActivityViewModel(LoginAccountActivityViewModel repoViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(LoginAccountFragmentViewModel.class)
    abstract ViewModel bindLoginAccountFragmentViewModel(LoginAccountFragmentViewModel repoViewModel);


    /* --- Main -- */
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel.class)
    abstract ViewModel bindMainViewModel(MainActivityViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeFramentViewModel.class)
    abstract ViewModel bindHomeFragmentViewModel(HomeFramentViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GroupFragmentViewModel.class)
    abstract ViewModel bindGroupsFragmentViewModel(GroupFragmentViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MessageFragmentViewModel.class)
    abstract ViewModel bindMesageFragmentViewModel(MessageFragmentViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProfileFragmentViewModel.class)
    abstract ViewModel bindProfileFragmentViewModel(ProfileFragmentViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MoreFragmentViewModel.class)
    abstract ViewModel bindMoreFragmentViewModel(MoreFragmentViewModel repoViewModel);
}
