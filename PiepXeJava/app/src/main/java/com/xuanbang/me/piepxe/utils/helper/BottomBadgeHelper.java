package com.xuanbang.me.piepxe.utils.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.xuanbang.me.piepxe.R;

public class BottomBadgeHelper {


    public static void showBadge(Context context, BottomNavigationView bottomNavigationView, @IdRes int itemId, @LayoutRes int layout, String value) {

        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        removeBadge(itemView);

        View badge = LayoutInflater.from(context).inflate(layout, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationItemView itemView) {
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }
}
