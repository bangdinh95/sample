package com.xuanbang.me.piepxe.ui.main.views;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.xuanbang.me.piepxe.R;
import com.xuanbang.me.piepxe.common.base.inf.OnBackPressHandel;
import com.xuanbang.me.piepxe.common.base.navigation.Navigator;
import com.xuanbang.me.piepxe.common.base.views.BaseActivityMVVM;
import com.xuanbang.me.piepxe.common.base.views.core.BaseActivity;
import com.xuanbang.me.piepxe.common.base.views.modules.BaseActivityModule;
import com.xuanbang.me.piepxe.databinding.ActivityMainBinding;
import com.xuanbang.me.piepxe.di.scopes.PerActivityScoped;
import com.xuanbang.me.piepxe.di.scopes.PerFragmentScoped;
import com.xuanbang.me.piepxe.ui.main.viewmodels.MainActivityViewModel;
import com.xuanbang.me.piepxe.ui.main.views.tabs.GroupsFragment;
import com.xuanbang.me.piepxe.ui.main.views.tabs.HomeFragment;
import com.xuanbang.me.piepxe.ui.main.views.tabs.MessageFragment;
import com.xuanbang.me.piepxe.ui.main.views.tabs.MoreFragment;
import com.xuanbang.me.piepxe.ui.main.views.tabs.ProfileFragment;
import com.xuanbang.me.piepxe.utils.helper.BottomBadgeHelper;

import javax.inject.Inject;

import dagger.Binds;
import dagger.android.ContributesAndroidInjector;

public class MainActivity extends BaseActivityMVVM<ActivityMainBinding, MainActivityViewModel> {

    private final static String TAG = MainActivity.class.getSimpleName();
    private final static int containerViewId = R.id.container;

    final Fragment homeFragment = new HomeFragment();

    Fragment groupsFragment;

    Fragment messageFragment;

    Fragment profileFragment;

    Fragment moreFragment;

    Fragment fragmentActive = homeFragment;


    @Inject
    Navigator mNavigator;


    @Override
    public int getLayoutId() {
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MainActivityViewModel.class);
        return R.layout.activity_main;
    }


    @Override
    public void initData(Bundle savedInstanceState) {

        //if savedInstanceState != null handel onRestoreInstanceState()
        if (savedInstanceState == null) {
            setupBottomNavigationWithFragments();
        }

        setupBadgeBottomNavigation();

//        mViewModel.getResults("bangdinh").observe(this, userModelResource -> {
//
//            if (userModelResource != null && userModelResource.data != null) {
//                Log.e(TAG, userModelResource.toString());
//            }
//
//        });
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        setupBottomNavigationWithFragments();
    }

    private void setupBottomNavigationWithFragments() {
//        addFragment(containerViewId, messageFragment, messageFragment, MessageFragment.TAG);
//        addFragment(containerViewId, profileFragment, profileFragment, ProfileFragment.TAG);
//        addFragment(containerViewId, moreFragment, moreFragment, MoreFragment.TAG);
        addFragment(containerViewId, homeFragment, HomeFragment.TAG);

        mViewDataBinding.navigationBottomViewMain.setOnNavigationItemSelectedListener(bottomNavigationItemSelected);
    }


    private void setupBadgeBottomNavigation() {
//        Fade fade = (Fade) TransitionInflater.from(this).inflateTransition(R.transition.activity_fade);
//        getWindow().setExitTransition(fade);
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

//        BottomBadgeHelper.showBadge(this, mViewDataBinding.navigationBottomViewMain, R.id.home,R.layout.layout_news_badge, "10+");
        BottomBadgeHelper.showBadge(this, mViewDataBinding.navigationBottomViewMain, R.id.groups, R.layout.layout_news_badge, "9+");
        BottomBadgeHelper.showBadge(this, mViewDataBinding.navigationBottomViewMain, R.id.message, R.layout.layout_news_badge, "2");
    }


    /**
     * Đầu tiên kiểm tra xem tab đang bấm đã có chưa
     * Chưa có thì khởi tạo
     * Có rồi thì show hide
     */
    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavigationItemSelected = item -> {
        switch (item.getItemId()) {
            case R.id.home:
                if (fragmentActive instanceof HomeFragment) return true;

                showHideFragment(fragmentActive, homeFragment);
                fragmentActive = homeFragment;
                return true;

            case R.id.groups:

                if (fragmentActive instanceof GroupsFragment) return true;

                if (groupsFragment == null) {
                    groupsFragment = new GroupsFragment();
                    addFragmentBottomSelected(containerViewId, groupsFragment, fragmentActive, GroupsFragment.TAG);
                    fragmentActive = groupsFragment;
                    return true;
                }
                showHideFragment(fragmentActive, groupsFragment);
                fragmentActive = groupsFragment;
                return true;

            case R.id.message:
                if (fragmentActive instanceof MessageFragment) return true;

                if (messageFragment == null) {
                    messageFragment = new MessageFragment();
                    addFragmentBottomSelected(containerViewId, messageFragment, fragmentActive, MessageFragment.TAG);
                    fragmentActive = messageFragment;
                    return true;
                }

                showHideFragment(fragmentActive, messageFragment);
                fragmentActive = messageFragment;
                return true;

            case R.id.profile:
                if (fragmentActive instanceof ProfileFragment) return true;

                if (profileFragment == null) {
                    profileFragment = new ProfileFragment();
                    addFragmentBottomSelected(containerViewId, profileFragment, fragmentActive, ProfileFragment.TAG);
                    fragmentActive = profileFragment;
                    return true;
                }

                showHideFragment(fragmentActive, profileFragment);
                fragmentActive = profileFragment;
                return true;

            case R.id.more:
                if (fragmentActive instanceof MoreFragment) return true;

                if (moreFragment == null) {
                    moreFragment = new MoreFragment();
                    addFragmentBottomSelected(containerViewId, moreFragment, fragmentActive, MoreFragment.TAG);
                    fragmentActive = moreFragment;
                    return true;
                }

                showHideFragment(fragmentActive, moreFragment);
                fragmentActive = moreFragment;
                return true;
        }
        return false;
    };

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed() " + isTaskRoot());
        if (isTaskRoot()) {
            //Sending Activity runing background using Intent startMain = new Intent(Intent.ACTION_MAIN);
            //startMain.addCategory(Intent.CATEGORY_HOME);
            //startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //startActivity(startMain);
            //or
            moveTaskToBack(true);

        } else {
            super.onBackPressed();
        }


    }

    private boolean handelFragmentOnBackPressed() {
        Fragment currentFragment = getCurrentFragment(R.id.container);
        return (currentFragment instanceof OnBackPressHandel && ((OnBackPressHandel) currentFragment).onBackPressed());
    }

    @dagger.Module(includes = BaseActivityModule.class)
    public abstract static class Module {

        @Binds
        @PerActivityScoped
        abstract BaseActivity bindBaseActivity(MainActivity activity);

        @PerFragmentScoped
        @ContributesAndroidInjector(modules = HomeFragment.Module.class)
        abstract HomeFragment bindHomeFragment();


        @PerFragmentScoped
        @ContributesAndroidInjector(modules = GroupsFragment.Module.class)
        abstract GroupsFragment bindGroupsFragment();


        @PerFragmentScoped
        @ContributesAndroidInjector(modules = MessageFragment.Module.class)
        abstract MessageFragment bindMessageFragment();


        @PerFragmentScoped
        @ContributesAndroidInjector(modules = ProfileFragment.Module.class)
        abstract ProfileFragment bindProfileFragment();


        @PerFragmentScoped
        @ContributesAndroidInjector(modules = MoreFragment.Module.class)
        abstract MoreFragment bindMoreFragment();
    }
}
