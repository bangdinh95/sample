package com.xuanbang.me.piepxe.ui.main.views;

import android.util.Log;

import com.xuanbang.me.piepxe.di.scopes.PerFragmentScoped;

import javax.inject.Inject;

@PerFragmentScoped
public final class FragmentDependencyHome {

    private final static String TAG = FragmentDependencyHome.class.getSimpleName();

    @Inject
    public FragmentDependencyHome() {
        Log.e(TAG, "Inject");
    }


}
