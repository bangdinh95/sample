package com.xuanbang.me.piepxe.common.base.views.core;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.xuanbang.me.piepxe.R;
import com.xuanbang.me.piepxe.common.base.views.modules.BaseActivityModule;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    @Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER)
    @Inject
    protected FragmentManager fragmentManager;


    @Inject
    protected BehaviorSubject<Object> behaviorSubject;

    protected final void addFragment(@IdRes int containerViewId, @NonNull Fragment fragment) {
        fragmentManager.beginTransaction()
                .add(containerViewId, fragment).commit();
    }

    protected final void addFragment(@IdRes int containerViewId, @NonNull Fragment fragment, String tag) {
        fragmentManager.beginTransaction()
                .add(containerViewId, fragment, tag).commit();
    }

    protected final void addFragment(@IdRes int containerViewId, @NotNull Fragment add, @NonNull Fragment hide, String tag) {
        fragmentManager.beginTransaction().add(containerViewId, add, tag).hide(hide).commit();
    }

    protected final void addFragmentBottomSelected(@IdRes int containerViewId, @NonNull Fragment add, @NonNull Fragment hide, String tag) {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .add(containerViewId, add, tag)
                .hide(hide)
                .commit();
    }

    protected final void showHideFragment(Fragment hide, Fragment show) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .hide(hide).show(show).commit();
    }

    protected final void replaceFragment(@IdRes int containerViewId, @NonNull Fragment fragment) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(
                        R.anim.abc_grow_fade_in_from_bottom,
                        R.anim.abc_fade_out,
                        R.anim.abc_fade_in,
                        R.anim.abc_shrink_fade_out_from_bottom)
                .replace(containerViewId, fragment)
                .addToBackStack(null).commit();
    }

    protected final void removeFragment(@NonNull Fragment fragment) {
        fragmentManager.beginTransaction().remove(fragment).commit();
    }

    protected final Fragment getCurrentFragment(@IdRes int containerViewId) {
        return fragmentManager.findFragmentById(containerViewId);
    }

    protected Disposable subscribe(@NotNull Consumer<Object> action) {
        return behaviorSubject.subscribe(action);
    }

    protected void publish(@NotNull Object message) {
        behaviorSubject.onNext(message);
    }
}
