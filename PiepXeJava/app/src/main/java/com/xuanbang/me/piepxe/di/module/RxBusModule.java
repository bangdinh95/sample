package com.xuanbang.me.piepxe.di.module;

import javax.inject.Singleton;

import dagger.Provides;
import io.reactivex.subjects.AsyncSubject;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;

@dagger.Module
public class RxBusModule {

    @Singleton
    @Provides
    PublishSubject<Object> providePublishSubject() {
        return PublishSubject.create();
    }

    @Singleton
    @Provides
    ReplaySubject<Object> provideReplaySubject() {
        return ReplaySubject.create();
    }

    @Singleton
    @Provides
    BehaviorSubject<Object> provideBehaviorSubject() {
        return BehaviorSubject.create();
    }

    @Singleton
    @Provides
    AsyncSubject<Object> provideAsyncSubject() {
        return AsyncSubject.create();
    }

}
