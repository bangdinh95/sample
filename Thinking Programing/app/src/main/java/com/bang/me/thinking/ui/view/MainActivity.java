package com.bang.me.thinking.ui.view;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.bang.me.thinking.BuildConfig;
import com.bang.me.thinking.R;
import com.bang.me.thinking.common.activity.BaseActivity;
import com.bang.me.thinking.databinding.ActivityMainBinding;
import com.bang.me.thinking.ui.inf.OnBackPressHandler;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int MAIN_ACTIVITY_FRAGMENT_CONTAINER_ID = R.id.container;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            addFragment(MAIN_ACTIVITY_FRAGMENT_CONTAINER_ID, MainFragment.newInstance());
        }
        String API_KEY = BuildConfig.API_KEY;
        Log.e(TAG, "API_KEY " + API_KEY);

        array();
    }

    int rows = 4;
    int column = 3;

    private void array() {

        int[][] arr = new int[rows][column];//[dòng][cột]

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < column; j++) {

                arr[i][j] = i + j;
            }
        }

//        for (int i = 0; i < column; i++) {
//            Log.e(">>Ban dau ", arr[0][i] + "\t");
////            for (int j = 0; j < column; j++) {
////                Log.e(">>Ban dau ", arr[i][j] + "\t");
////            }
//        }

        chuoiDaoNguoc(arr);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < column; j++) {
                Log.e(">>Sau ", arr[i][j] + "\t");
            }
        }
    }

    //4 cot 3 hang
    private void chuoiDaoNguoc(int[][] arr) {

        for (int i = 0; i < rows / 2; i++) {

            for (int j = 0; j < column; j++) {

                int temp = arr[i][j];
                arr[i][j] = arr[rows - 1 - i][j];
                arr[rows - 1 - i][j] = temp;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed");
        if (handelFragmentOnBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    public boolean handelFragmentOnBackPressed() {
        Fragment fragmentCurrent = getFragmentCurrent(MAIN_ACTIVITY_FRAGMENT_CONTAINER_ID);
        return fragmentCurrent instanceof OnBackPressHandler && ((OnBackPressHandler) fragmentCurrent).onBackPress();
    }
}
