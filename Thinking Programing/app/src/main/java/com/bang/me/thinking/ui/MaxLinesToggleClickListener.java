package com.bang.me.thinking.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.transition.ChangeBounds;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

public class MaxLinesToggleClickListener implements View.OnClickListener {

    private int collapsedLines;
    private final Transition transition = new ChangeBounds().setDuration(200).setInterpolator(new FastOutSlowInInterpolator());



    public MaxLinesToggleClickListener(int collapsedLines) {
        this.collapsedLines = collapsedLines;
    }

    @Override
    public void onClick(View view) {

        TransitionManager.beginDelayedTransition(findParent(view), transition);
        TextView textView = (TextView) view;
        textView.setMaxLines(textView.getMaxLines() > collapsedLines ? collapsedLines : Integer.MAX_VALUE);
    }

    private ViewGroup findParent(View view) {
        View parentView = view;

        while (parentView != null) {
            View parent = (View) parentView.getParent();
            if (parent instanceof ConstraintLayout) {
                return (ViewGroup) parent;
            }
            parentView = parent;
        }
        // If we reached here we didn't find a RecyclerView in the parent tree, so lets just use our parent
        return (ViewGroup) view.getParent();
    }
}
