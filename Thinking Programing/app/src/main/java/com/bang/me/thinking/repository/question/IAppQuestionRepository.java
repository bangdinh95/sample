package com.bang.me.thinking.repository.question;

import androidx.annotation.RawRes;

import com.bang.me.thinking.ui.model.Question;

import io.reactivex.Flowable;

public interface IAppQuestionRepository {
    Flowable<Question> getListUnit(@RawRes int id);
}
