package com.bang.me.thinking.util;

import android.content.Context;

import androidx.annotation.RawRes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.Primitives;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import io.reactivex.Flowable;

public final class ReadFileJsonHelper {


    public static String getRawJson(Context context, @RawRes int id) {

        try {
            InputStream inputStream = context.getResources().openRawResource(id);
            int size = inputStream.available();
            byte[] buf = new byte[size];
            inputStream.read(buf);
            inputStream.close();
            return new String(buf, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static <T> Flowable<T> getJsonData(String json, Class<T> classOfT) {
        Gson gson = new GsonBuilder().create();
        Object object = gson.fromJson(json, (Type) classOfT);
        return Flowable.just(Objects.requireNonNull(Primitives.wrap(classOfT).cast(object)));
    }
}
