package com.bang.me.thinking.interactor;

import androidx.core.util.Preconditions;

import com.bang.me.thinking.excutor.AppSchedulerProvider;
import com.bang.me.thinking.excutor.SchedulerProvider;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.DisposableSubscriber;

public abstract class UseCase<T, Params> {

    private SchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    public UseCase() {
        this.schedulerProvider = new AppSchedulerProvider();
        this.compositeDisposable = new CompositeDisposable();
    }

    abstract Flowable<T> buildUseCaseWithFlowable(Params params);


    public void excute(DisposableSubscriber<T> subscriber, Params params) {
        Preconditions.checkNotNull(subscriber);
        if (subscriber.isDisposed()) {
            subscriber.dispose();
        }
        final Flowable<T> flowable = this.buildUseCaseWithFlowable(params)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui());
        addDisposable(flowable.subscribeWith(subscriber));
    }

    /**
     * Dispose from current {@link CompositeDisposable}
     */
    public void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    /**
     * Dispose from current {@link CompositeDisposable}
     */
    public void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(compositeDisposable);
        compositeDisposable.add(disposable);
    }
}
