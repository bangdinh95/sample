package com.bang.me.thinking.ui.adapter;

import com.bang.me.thinking.R;
import com.bang.me.thinking.common.adapter.BaseAdapter;
import com.bang.me.thinking.common.adapter.DataBindingViewHolder;
import com.bang.me.thinking.databinding.ItemThinkingBinding;
import com.bang.me.thinking.ui.inf.MaxLinesToggleClickListener;
import com.bang.me.thinking.ui.inf.Presenter;
import com.bang.me.thinking.ui.model.Unit;
import com.bang.me.thinking.ui.model.User;

public class ThinkingAdapter extends BaseAdapter<Unit> {


    public User mUser = new User();
    private Presenter mPresenter;
    private MaxLinesToggleClickListener mLinesToggleClickListener;

    public void setUser(User user) {
        mUser = user;
    }

    public void setLinesToggleClickListener(MaxLinesToggleClickListener linesToggleClickListener) {
        mLinesToggleClickListener = linesToggleClickListener;
    }

    public void setPresenter(Presenter presenter) {
        mPresenter = presenter;
    }

    public ThinkingAdapter() {
        super(Unit.DIFF_CALLBACK);
    }

    @Override
    public int getItemViewType(int position) {
//        if (getItem(position).id == 1 || getItem(position).id == 2 || getItem(position).id == 3)
//            return R.layout.item_thinking;
//        return R.layout.item_row;
        return R.layout.item_thinking;
    }


    @Override
    public void bindView(DataBindingViewHolder viewHolder) {
        ItemThinkingBinding thinkingBinding = (ItemThinkingBinding) viewHolder.mBinding;
        thinkingBinding.setHandel(mPresenter);
        thinkingBinding.setMaxLineToggleClick(mLinesToggleClickListener);
        thinkingBinding.setUser(mUser);
    }
}
