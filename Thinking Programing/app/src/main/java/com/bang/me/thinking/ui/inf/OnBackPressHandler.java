package com.bang.me.thinking.ui.inf;

public interface OnBackPressHandler {
    boolean onBackPress();
}
