package com.bang.me.thinking.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.ChangeBounds;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.bang.me.thinking.R;
import com.bang.me.thinking.common.fragment.BaseFragmentMVVM;
import com.bang.me.thinking.databinding.MainFragmentBinding;
import com.bang.me.thinking.ui.adapter.ThinkingAdapter;
import com.bang.me.thinking.ui.inf.MaxLinesToggleClickListener;
import com.bang.me.thinking.ui.inf.Presenter;
import com.bang.me.thinking.ui.model.Unit;
import com.bang.me.thinking.ui.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseFragmentMVVM<MainActivity, MainFragmentBinding, MainViewModel> implements View.OnClickListener, Presenter, MaxLinesToggleClickListener {

    private final static String TAG = MainFragment.class.getSimpleName();
    private ThinkingAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    protected void intitData(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        configureView();
        setupToolbar();
        setupRecyclerView();

    }


    private void configureView() {

    }

    private void setupToolbar() {
        activity.setSupportActionBar(mViewBinding.layoutToolbar.toolbar);
        actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_toolbar));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }


    private void setupRecyclerView() {

        adapter = new ThinkingAdapter();
//        mViewBinding.recyclerList.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
        mViewBinding.recyclerList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mViewBinding.recyclerList.setAdapter(adapter);

        adapter.setPresenter(this);
        adapter.setLinesToggleClickListener(this);

        mViewModel.getUsersList().observe(this, units -> {
            List<Unit> newList = new ArrayList<>(units);
            adapter.submitList(newList);

        });
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.main_fragment;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_option) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e(TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

    /**
     * Click View in Adapter
     */
    @Override
    public void onClick(View view, Object obj) {
        if (obj instanceof Unit) {
            Unit unit = (Unit) obj;
            activity.replaceFragment(MainActivity.MAIN_ACTIVITY_FRAGMENT_CONTAINER_ID, new AnswerFragment(), this, "MainFragment");
//            switch (unit.id) {
//                case 1:
//                    break;
//                case 2:
//                    break;
//                case 3:
//                    break;
//                case 4:
//                    break;
//                case 5:
//                    break;
//            }
        }
    }


    /**/
    @Override
    public void onExpandCollapse(View view, int collapseLine) {


        TransitionManager.beginDelayedTransition(findParent(view), transition);
        TextView textView = (TextView) view;
        Log.e(TAG, "collapseLine: " + textView.getMaxLines());
        boolean display = textView.getMaxLines() > collapseLine;
        textView.setMaxLines(display ? collapseLine : Integer.MAX_VALUE);
        adapter.mUser.getName().set(display);
    }

    private final Transition transition = new ChangeBounds().setDuration(200).setInterpolator(new FastOutSlowInInterpolator());

    private ViewGroup findParent(View view) {
        View parentView = view;

        while (parentView != null) {
            View parent = (View) parentView.getParent();
            if (parent instanceof ConstraintLayout) {
                return (ViewGroup) parent;
            }
            parentView = parent;
        }
        // If we reached here we didn't find a RecyclerView in the parent tree, so lets just use our parent
        return (ViewGroup) view.getParent();
    }
}
