package com.bang.me.thinking.common.activity;

import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.bang.me.thinking.R;

public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {
    protected T mBindingView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBindingView = DataBindingUtil.setContentView(this, getLayoutResource());
        initData(savedInstanceState);
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    protected abstract void initData(Bundle savedInstanceState);

    public void addFragment(@IdRes int containerViewId, @NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(containerViewId, fragment).commit();
    }

    public void replaceFragment(@IdRes int containerViewId, @NonNull Fragment show, Fragment hide, String tag) {
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.abc_grow_fade_in_from_bottom,
                R.anim.abc_fade_out,
                R.anim.abc_fade_in,
                R.anim.abc_shrink_fade_out_from_bottom)
                .replace(containerViewId, show, tag).hide(hide).addToBackStack(null).commit();
    }

    public Fragment getFragmentCurrent(@IdRes int containerViewId) {
        return getSupportFragmentManager().findFragmentById(containerViewId);
    }
}
