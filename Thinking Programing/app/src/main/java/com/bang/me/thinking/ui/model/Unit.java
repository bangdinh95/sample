package com.bang.me.thinking.ui.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class Unit {
    @SerializedName("id")
    public int id;
    @SerializedName("title")
    public String title;
    @SerializedName("content")
    public String content;




    public Unit(int id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;

    }

    public static DiffUtil.ItemCallback<Unit> DIFF_CALLBACK = new DiffUtil.ItemCallback<Unit>() {
        @Override
        public boolean areItemsTheSame(@NonNull Unit oldItem, @NonNull Unit newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Unit oldItem, @NonNull Unit newItem) {
            return oldItem.equals(newItem);
        }
    };




    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        Unit unit = (Unit) obj;
        return unit.id == id && unit.title.equals(title) && unit.content.equals(content);
    }

    @NonNull
    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
}
