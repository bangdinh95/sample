package com.bang.me.thinking.repository.question;

import android.content.Context;

import com.bang.me.thinking.ui.model.Question;
import com.bang.me.thinking.util.ReadFileJsonHelper;

import io.reactivex.Flowable;

public class AppQuestionRepository implements IAppQuestionRepository {

    private Context context;

    public AppQuestionRepository(Context context) {
        this.context = context;
    }

    @Override
    public Flowable<Question> getListUnit(int id) {
        String json = ReadFileJsonHelper.getRawJson(context, id);
        return ReadFileJsonHelper.getJsonData(json, Question.class);
    }
}
