package com.bang.me.thinking.common.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;

import com.bang.me.thinking.common.viewmodel.BaseViewModel;

public abstract class BaseFragmentMVVM<X extends AppCompatActivity, T extends ViewDataBinding, VM extends BaseViewModel> extends BaseFragment<T, X> {
    protected VM mViewModel;
}
