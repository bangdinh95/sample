package com.bang.me.thinking.common.adapter;

import android.content.res.Resources;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.bang.me.thinking.ui.MaxLinesToggleClickListener;

public class ThinkingMainBindingAdapter {

    //https://github.com/chrisbanes/tivi/blob/master/app/src/main/java/app/tivi/ui/databinding/TiviBindingAdapters.kt

    @BindingAdapter("format")
    public static void formatText(TextView textView, int id) {
        Resources resources = textView.getContext().getResources();

    }


    //app:maxLinesToggle="@{@integer/details_summary_collapsed_lines}"
    @BindingAdapter(value = {"maxLinesToggle"})
    public static void maxLinesClickListener(TextView view, int oldCollapsedMaxLines, int newCollapsedMaxLines) {
//        Log.e("oldCollapsedMaxLines: ", ">> " + oldCollapsedMaxLines);
//        Log.e("newCollapsedMaxLines: ", ">> " + newCollapsedMaxLines);
        if (oldCollapsedMaxLines != newCollapsedMaxLines) {
            // Default to collapsed
            newCollapsedMaxLines = view.getMaxLines();
//            Log.e("GetLines: ", ">> " + newCollapsedMaxLines);
            // Now set click listener
            view.setOnClickListener(new MaxLinesToggleClickListener(newCollapsedMaxLines));
        }
    }

    /**
     *  @JvmStatic
     *     @InverseBindingAdapter(attribute = "textChange", event = "textAttrChanged")
     *     fun getTextChange(view: EditText): String {
     *         val text = view.text.toString().replace("\\D+".toRegex(), "")
     *         if (TextUtils.isEmpty(text))  {
     *             return "0"
     *         }
     *         val number = Double.parseDouble(text)
     *         return String.format("%,.0f", number)
     *     }
     *
     *     @JvmStatic
     *     @BindingAdapter(value = *arrayOf("textChange", "textAttrChanged"), requireAll = false)
     *     fun setTextChange(view: EditText, text: String, textAttrChanged: InverseBindingListener) {
     *         view.addTextChangedListener(object : TextWatcher {
     *             override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
     *                 //TODO nothing
     *             }
     *
     *             override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
     *                 textAttrChanged.onChange()
     *                 view.setSelection(view.text.length)
     *             }
     *
     *             override fun afterTextChanged(editable: Editable) {
     *                 //TODO nothing
     *             }
     *         })
     *     }
     *     */


}
