package com.bang.me.thinking.common.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.bang.me.thinking.BR;


public class DataBindingViewHolder extends RecyclerView.ViewHolder {
    public final ViewDataBinding mBinding;

    public DataBindingViewHolder(@NonNull ViewDataBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(Object obj) {
        mBinding.setVariable(BR.obj, obj);
        mBinding.executePendingBindings();
    }
}
