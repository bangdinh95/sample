package com.bang.me.thinking.ui.view;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.bang.me.thinking.R;
import com.bang.me.thinking.common.fragment.BaseFragment;
import com.bang.me.thinking.databinding.FragmentAnswerBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnswerFragment extends BaseFragment<FragmentAnswerBinding, MainActivity> {


    public AnswerFragment() {
        // Required empty public constructor
    }


    @Override
    protected void intitData(Bundle savedInstanceState) {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_answer;
    }

}
