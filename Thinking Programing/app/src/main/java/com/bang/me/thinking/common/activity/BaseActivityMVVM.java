package com.bang.me.thinking.common.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.bang.me.thinking.common.viewmodel.BaseViewModel;

public abstract class BaseActivityMVVM<T extends ViewDataBinding, VM extends BaseViewModel> extends BaseActivity<T> {

    protected VM mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
