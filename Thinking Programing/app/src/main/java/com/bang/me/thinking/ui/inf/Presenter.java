package com.bang.me.thinking.ui.inf;

import android.view.View;

public interface Presenter {
    void onClick(View view, Object obj);
}
