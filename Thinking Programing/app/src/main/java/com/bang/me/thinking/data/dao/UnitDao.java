package com.bang.me.thinking.data.dao;

import androidx.lifecycle.LiveData;

import com.bang.me.thinking.ui.model.Unit;

import java.util.List;

public interface UnitDao {
    LiveData<List<Unit>> unitByLastName();
}
