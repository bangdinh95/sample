package com.bang.me.thinking.ui.viewmodel;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bang.me.thinking.R;
import com.bang.me.thinking.common.viewmodel.BaseViewModel;
import com.bang.me.thinking.interactor.QuestionCase;
import com.bang.me.thinking.ui.model.Question;
import com.bang.me.thinking.ui.model.Unit;

import java.util.List;

import io.reactivex.subscribers.DisposableSubscriber;

public class MainViewModel extends BaseViewModel {

    private final static String TAG = MainViewModel.class.getSimpleName();
    public MutableLiveData<List<Unit>> usersList;
    private QuestionCase mQuestionCase;
    private final static int idRaw = R.raw.questions;
    public boolean isShow = true;

    public MainViewModel(@NonNull Application application) {
        super(application);
        Context appContext = application.getApplicationContext();
        this.mQuestionCase = new QuestionCase(appContext);
    }


    public LiveData<List<Unit>> getUsersList() {
//        Transformations.switchMap(usersList,)
        if (usersList == null) {
            usersList = new MutableLiveData<>();
        }
        this.mQuestionCase.excute(new GetQuestion(), idRaw);

        return usersList;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.e(TAG, "onCleared()");
        mQuestionCase.dispose();
    }

    private final class GetQuestion extends DisposableSubscriber<Question> {
        @Override
        public void onNext(Question question) {
            Log.e(TAG, "onNext");
            usersList.setValue(question.question);
        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG, "onError");
        }

        @Override
        public void onComplete() {
            Log.e(TAG, "onComplete");

        }
    }
}
