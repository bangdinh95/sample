package com.bang.me.thinking.ui.model;

import androidx.databinding.ObservableField;

public class User {
    public static ObservableField<Boolean> name = new ObservableField<>();

    public ObservableField<Boolean> getName() {
        return name;
    }
}
