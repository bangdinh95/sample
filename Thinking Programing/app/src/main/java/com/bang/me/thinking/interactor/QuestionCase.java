package com.bang.me.thinking.interactor;

import android.content.Context;

import com.bang.me.thinking.repository.question.AppQuestionRepository;
import com.bang.me.thinking.repository.question.IAppQuestionRepository;
import com.bang.me.thinking.ui.model.Question;

import io.reactivex.Flowable;

public class QuestionCase extends UseCase<Question, Integer> {
    private IAppQuestionRepository appQuestionRepository;


    public QuestionCase(Context context) {
        super();
        this.appQuestionRepository = new AppQuestionRepository(context);
    }

    @Override
    Flowable<Question> buildUseCaseWithFlowable(Integer integer) {
        return appQuestionRepository.getListUnit(integer);
    }
}
