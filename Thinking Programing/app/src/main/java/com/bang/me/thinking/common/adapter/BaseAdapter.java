package com.bang.me.thinking.common.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

public abstract class BaseAdapter<T> extends ListAdapter<T, DataBindingViewHolder> {


    protected BaseAdapter(DiffUtil.ItemCallback<T> diff) {
        super(diff);
    }

    @NonNull
    @Override
    public DataBindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, viewType, parent, false);
        return new DataBindingViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull DataBindingViewHolder holder, int position) {
        holder.bind(getItem(position));
        bindView(holder);
    }

    public abstract void bindView(DataBindingViewHolder viewHolder);

}
