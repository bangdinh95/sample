package com.bang.me.thinking.ui.inf;

import android.view.View;

public interface MaxLinesToggleClickListener {
    void onExpandCollapse(View view, int collapseLine);
}
